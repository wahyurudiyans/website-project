<?php
    include '../web_machine/eng_db_con.php';
    $user   = $_SESSION['username'];
    $query  = "SELECT * FROM halcen_user WHERE username='$user'";
    
    $result = mysqli_query($con, $query);
    $data   = mysqli_fetch_array($result);
?>

<html>
    <body>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="card card-user">
                            <div class="image">
                                <img src="assets/img/background.jpg" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                  <img class="avatar border-white" src="assets/img/faces/face-2.jpg" alt="..."/>
                                  <h4 class="title"><?php echo $data['nama_depan'] ?><br />
                                     <a href="#"><small><?php echo $data['email'] ?></small></a>
                                  </h4>
                                </div>
                                <p class="description text-center">
                                    "I like the way you work it <br>
                                    No diggity <br>
                                    I wanna bag it up"
                                </p>
                            </div>                            
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Profile</h4>
                            </div>                       
                            
                            <?php include 'messages.php'; ?>
                            <div class="content">
                                <form action='../web_machine/eng_update_user.php' method="POST">
                                    <input type="hidden" name='id' value="<?php echo $data['id'];?>">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Institusi</label>
                                                <input type="text" class="form-control border-input" disabled placeholder="Company" value="Tanjungpura University">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input name='username' type="text" class="form-control border-input" placeholder="Username" value="<?php echo $data['username'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Email</label>
                                                <input name='email' type="email" class="form-control border-input" placeholder="Email" value="<?php echo $data['email'] ?>">
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Depan</label>
                                                <input name='nama_depan' type="text" class="form-control border-input" placeholder="Nama Depan" value="<?php echo $data['nama_depan'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Belakang</label>
                                                <input name='nama_belakang' type="text" class="form-control border-input" placeholder="Nama Belakang" value="<?php echo $data['nama_belakang'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Password Lama</label>
                                                <input name='password' type="password" class="form-control border-input" placeholder="Old Password">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Password Baru</label>
                                                <input name='new_password' type="password" class="form-control border-input" placeholder="New Password">
                                            </div>
                                        </div>      
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Ulangi Password</label>
                                                <input name='pass_check' type="password" class="form-control border-input" placeholder="Repeat Password" >
                                            </div>
                                        </div>        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Role</label>
                                                <select name='role' class="form-control border-input">
                                                    <option value="<?php echo $data['role'] ?>"><?php echo $data['role'] ?></option>
                                                    <option value="Administrator">Administrator</option>
                                                    <option value="General">General User</option>
                                                    <option value="Author">Author</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Jabatan</label>
                                                <input name='jabatan' type="text" class="form-control border-input" placeholder="Jabatan" value="<?php echo $data['jabatan'] ?>">
                                            </div>
                                        </div>
                                        <div class="text-center" style="margin-top: 33px;">
                                            <button type="submit" class="btn btn-info btn-fill btn-wd">Update Profile</button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>