<?php
    session_start();

    if (!isset($_SESSION['status'])) {
        # code...
        $_SESSION['status'] = 'ready';
    }

    include '../web_machine/eng_db_con.php';

    if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
        # code...
        header('Location:/halalcenter/page/assalammualaikum.php');
        exit;
    }
    
    $user   = $_SESSION['username'];
    $query  = "SELECT * FROM halcen_user WHERE username='$user'";
    $result = mysqli_query($con, $query);
    $data   = mysqli_fetch_array($result);
?>

<!Doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Paper Dashboard by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- User CSS link -->
    <link rel="stylesheet" href="../assets/css/design.css">
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="black" data-active-color="success">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Halal Center Untan
                </a>
            </div>

            <ul class="nav">
                <li
                    <?php
                        if ($_GET['laman'] == 'dashboard') {
                            # code...
                            echo 'class="active"';
                        }
                    ?>
                    >
                    <a href="?laman=dashboard">
                        <i class="ti-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li
                    <?php
                        if ($_GET['laman'] == 'post') {
                            # code...
                            echo 'class="active"';
                        }
                    ?>                
                    >
                    <a href="?laman=post">
                        <i class="ti-pencil"></i>
                        <p>Buat Postingan</p>
                    </a>
                </li>  
                <li
                    <?php
                        if ($_GET['laman'] == 'edit_lu') {
                            # code...
                            echo 'class="active"';
                        }
                    ?>                
                    >
                    <a href="?laman=edit_lu">
                        <i class="ti-direction-alt"></i>
                        <p>Laman Utama</p>
                    </a>
                </li>
                <li
                    <?php
                        if ($_GET['laman'] == 'edit_pl') {
                            # code...
                            echo 'class="active"';
                        }
                    ?>                
                    >
                    <a href="?laman=edit_pl">
                        <i class="ti-pencil-alt"></i>
                        <p>Pelayanan</p>
                    </a>
                </li>
                <li
                    <?php
                        if ($_GET['laman'] == 'edit_pn') {
                            # code...
                            echo 'class="active"';
                        }
                    ?>                
                    >
                    <a href="?laman=edit_pn">
                        <i class="ti-write"></i>
                        <p>Penelitian</p>
                    </a>
                </li>
                <li
                    <?php
                        if ($_GET['laman'] == 'edit_fl') {
                            # code...
                            echo 'class="active"';
                        }
                    ?>                
                    >
                    <a href="?laman=edit_fl">
                        <i class="ti-briefcase"></i>
                        <p>Fasilitas</p>
                    </a>
                </li>                                          
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-bell"></i>
                                <p class="notification">5</p>
                                <p>Notifications</p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                            <li><a href="#">Notification 1</a></li>
                            <li><a href="#">Notification 2</a></li>
                            <li><a href="#">Notification 3</a></li>
                            <li><a href="#">Notification 4</a></li>
                            <li><a href="#">Another notification</a></li>
                            </ul>
                        </li>
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ti-user"></i>
								<p><?php echo $data['nama_depan'].' '.$data['nama_belakang']; ?></p>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="../web_machine/eng_logout.php?logout=true">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
<!-- infobar -->
                <?php 
                    if ($_GET['laman'] == 'dashboard') {
                        # code...
                        include 'infobar.php';
                    }
                ?>

                <div class="row">                
                    <?php
                        switch ($_GET['laman']) {
                            case 'post':
                                # code...
                                $title = "Tambahkan Postingan";
                                require_once 'post.php';
                                break;

                            case 'edit_lu':
                                # code...
                                $title = "Lakukan Pengaturan Halaman Utama";
                                require_once 'edt_utama.php';
                                break;     

                            case 'edit_pl':
                                # code...
                                $title = "Lakukan Pengaturan Halaman Pelayanan";
                                require_once 'edt_pelayanan.php';
                                break;                                                           

                            case 'edit_pn':
                                # code...
                                $title = "Lakukan Pengaturan Halaman Penelitian";
                                require_once 'edt_penelitian.php';
                                break; 

                            case 'edit_fl':
                                # code...
                                $title = "Lakukan Pengaturan Halaman Fasilitas";
                                require_once 'edt_fasilitas.php';
                                break;
                            
                            default:
                                # code...
                                require_once 'edt_profile.php';
                                break;
                        }
                    ?>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <li>
                            <a href="../">
                                Home
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="#">Wahyu Rudiyan S</a>
                </div>
            </div>
        </footer>
    </div>
</div>
</body>

    <?php if ($_GET['laman'] == 'post'):
        include 'edt_posts.php';
     endif ?>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
          </div>
          <div class="modal-body">
            <p>Some text in the modal.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- CKEditor -->
    <script src="assets/ckeditor/ckeditor.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

    <script type="text/javascript">
    // Script to Get Parameter from URL
        function getQueryVariable(variable)
        {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
                for (var i=0;i<vars.length;i++) {
                    var pair = vars[i].split("=");
                    if(pair[0] == variable)
                    {
                        return pair[1];
                    }
                }
            return(false);
        }
	</script>

</html>
