<?php 
    $query = "SELECT * FROM halcen_posts";
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
?>    
    <!-- Modal -->
    <div id="myModal<?php echo $row['id'];?>" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Postingan</h4>
                </div>
                <div class="modal-body">
                    <form action="../web_machine/eng_posts_update.php" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="lastId" value="<?php echo $row['id'];?>">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Judul Post</label>
                                    <input name="judulPost" type="text" class="form-control border-input" value="<?php echo $row['judulPost']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Ketegori</label>
                                    <select name="category" class="form-control border-input" name="kategori" id="kategori">
                                        <option value="<?php echo $row['category']; ?>"><?php echo $row['category'];?></option>
                                        <option value="Berita">Berita</option>
                                        <option value="Pengumuman">Pengumuman</option>
                                        <option value="Laporan">Laporan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Gambar</label>
                                    <input name="image" type="file" class="form-control border-input">
                                </div>
                            </div>                                                                        
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Deskripsi Postingan</label>
                                    <input type="text" name="deskripsi" class="form-control border-input" value="<?php echo $row['deskripsi']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Konten</label>
                                <textarea name="konten" class="ckeditor" id="ckeditor"><?php echo $row['konten']; ?></textarea>
                            </div>
                        </div>                                                                                    
                	</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Save</button>
                </div>
            </form>
        	</div>
        </div>
    </div>
<?php
    }
?>