<?php
    include '../web_machine/eng_db_con.php';

    $query  = "SELECT * FROM halcen_lamanutama";
    $result = mysqli_query($con, $query);
    $data   = mysqli_fetch_array($result);    
?>
<!DOCTYPE html>
<html>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
            	<div class="col-md-12">
            		<div class="card">
                        <div class="header">
                            <h4 class="title">
                        	<?php
								echo($title);								
                        	?>
                            </h4>                                                    
						</div>
					<?php include 'messages.php'; ?>						
            			<form action="../web_machine/eng_update_utama.php" method="POST" enctype="multipart/form-data">
							<div class="content">
		            			<div class="row">
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Logo Utama</label>
		            						<input  type="file" class="form-control border-input" name="logoUtama">
		            					</div>
		            				</div>
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Judul Utama</label>
		            						<input  type="text" class="form-control border-input" name="judulUtama" value="<?php echo $data['judulUtama'];?>">
		            					</div>
		            				</div>
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Deskripsi Judul</label>
		            						<input  type="text" class="form-control border-input" name="deskripsiJudul" value="<?php echo $data['deskripsiJudul'];?>">
		            					</div>
		            				</div> 	
		            			</div>
								<div class="row">
		            				<div class="col-md-12">
		            					<div class="form-group">
											<label>Judul Deskripsi Pendek</label>
		            						<input type="text" class="form-control border-input" name="judulDesPendek" value="<?php echo $data['judulDesPendek'];?>">
		            					</div>
		            				</div>	
		            			</div>		            			
								<div class="row">
		            				<div class="col-md-12">
		            					<div class="form-group">
											<label>Deskripsi Pendek</label>
		            						<textarea class="form-control border-input" rows="3" name="desPendek"><?php echo $data['desPendek'];?></textarea>
		            					</div>
		            				</div>	
		            			</div>			            			
								<div class="row">
		            				<div class="col-md-6">
		            					<div class="form-group">
											<label>Visi</label>
		            						<textarea class="form-control border-input" rows="3" name="visi"><?php echo $data['visi'];?></textarea>
		            					</div>
		            				</div>
		            				<div class="col-md-6">
		            					<div class="form-group">
											<label>Misi</label>
		            						<textarea class="form-control border-input" rows="3" name="misi"><?php echo $data['misi'];?></textarea>
		            					</div>
		            				</div>	
		            			</div>
								<div class="row">
		            				<div class="col-md-12">
		            					<div class="form-group">
											<label>Judul Deskripsi Pelayanan</label>
		            						<input type="text" class="form-control border-input" name="judulDesPelayanan" value="<?php echo $data['judulDesPelayanan'];?>">
		            					</div>
		            				</div>	
		            			</div>		            			
								<div class="row">
		            				<div class="col-md-12">
		            					<div class="form-group">
											<label>Isi Deskripsi Pelayanan</label>
		            						<textarea class="form-control border-input" rows="8" name="desPelayanan"><?php echo $data['desPelayanan'];?></textarea>
		            					</div>
		            				</div>	
		            			</div>
								<div class="row">
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Gambar 1</label>
		            						<input  type="file" class="form-control border-input" name="gbr1" >
		            					</div>
		            				</div>
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Gambar 2</label>
		            						<input  type="file" class="form-control border-input" name="gbr2">
		            					</div>
		            				</div>
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Gambar 3</label>
		            						<input  type="file" class="form-control border-input" name="gbr3">
		            					</div>
		            				</div>	
		            			</div>
		            			<div class="row">
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Telepon</label>
		            						<input  type="text" class="form-control border-input" name="telepon" value="<?php echo $data['telepon'];?>">
		            					</div>
		            				</div>
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Email</label>
		            						<input  type="text" class="form-control border-input" name="email" value="<?php echo $data['email'];?>">
		            					</div>
		            				</div>
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Alamat</label>
		            						<input  type="text" class="form-control border-input" name="alamat" value="<?php echo $data['alamat'];?>">
		            					</div>
		            				</div>	
		            			</div>
		            			<div class="row text-center">
		            				<button type="submit" class="btn btn-info btn-fill btn-w">Ubah Data Halaman</button>
		            			</div>		            						            			
							</div>          				
            			</form>
            		</div>
            	</div>
            </div>
        </div>
	</div>
	
</html>