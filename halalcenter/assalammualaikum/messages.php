<?php if ($_SESSION['status'] == 'berhasil') { ?>
    <div class="row justify-content-center">
        <div class="content">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="right: 5px;">X</a>
                    <strong>Data Berhasil di Update</strong>
                </div>
            </div>                              
        </div>
    </div>              
    <?php } 
        if ($_SESSION['status'] == 'gagal'){
    ?>
    <div class="row justify-content-center">
        <div class="content">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="right: 5px;">X</a>
                    <strong>Data Gagal di-Update</strong>
                </div>
            </div>                              
        </div>
    </div>
    <?php 
    }
?>

<?php if ($_SESSION['status'] == 'posted') { ?>
    <div class="row justify-content-center">
        <div class="content">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="right: 5px;">X</a>
                    <strong>Postingan Telah di-Publikasi</strong>
                </div>
            </div>                              
        </div>
    </div>              
    <?php } 
    if ($_SESSION['status'] == 'unposted'){
    ?>
    <div class="row justify-content-center">
        <div class="content">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="right: 5px;">X</a>
                    <strong>Postingan Telah di-Publikasi</strong>
                </div>
            </div>                              
        </div>
    </div>
    <?php 
    }

    $_SESSION['status'] = '';
?>