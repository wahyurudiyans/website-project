<?php
    include '../web_machine/eng_db_con.php';    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header">
                            <h4 class="title">Buat Postingan Baru</h4>                        
                        </div> 
                        <?php include 'messages.php'; ?>
                        <div class="content">
                            <form action="../web_machine/eng_posts.php" method="POST" enctype="multipart/form-data">                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Judul Post</label>
                                            <input name="judulPost" type="text" class="form-control border-input" placeholder="Company">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Ketegori</label>
                                            <select name="category" class="form-control border-input" name="kategori" id="kategori">
                                                <option value="Berita">Berita</option>
                                                <option value="Pengumuman">Pengumuman</option>
                                                <option value="Laporan">Laporan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
		            					<div class="form-group">
											<label>Gambar</label>
		            						<input name="image" type="file" class="form-control border-input">
		            					</div>
		            				</div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Deskripsi Postingan</label>
                                            <input type="text" name="deskripsi" class="form-control border-input">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Konten</label>
                                        <textarea name="konten" class="ckeditor" id="ckeditor"></textarea>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 text-center" styles="margin-top:50px;">
                                    <button type="submit" class="btn btn-info btn-fill btn-wd">Posting</button>
                                </div>                        
                            </form>                        
                        </div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="header">
                                            <h4 class="title">Postingan</h4>           
                                        </div>
                                        <div class="content table-responsive table-full-width">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr><th>No</th>
                                                    <th>Judul</th>
                                                    <th>Deskripsi</th>
                                                    <th>Kategori</th>
                                                    <th>Opsi</th>
                                                </tr></thead>
                                                <tbody>
                                                <?php 
                                                        $query = "SELECT * FROM halcen_posts";
                                                        $result = mysqli_query($con, $query);
                                                    while($row = mysqli_fetch_array($result)){
                                                ?>
                                                    <tr>
                                                        <td><?php echo $row['id']; ?></td>
                                                        <td><?php echo $row['judulPost']; ?></td>
                                                        <td><?php echo $row['deskripsi']; ?></td>
                                                        <td><?php echo $row['category']; ?></td>
                                                        <td class="text-center">
                                                            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#myModal<?php echo $row['id']; ?>">Edit</button>                                                            
                                                            <button type="button" class="btn btn-danger btn-block">Hapus</button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>               
                </div>
            </div>               
        </div> 
    </div>
</body>
</html>