<!DOCTYPE html>
<html>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
            	<div class="col-md-12">
            		<div class="card">
                        <div class="header">
                            <h4 class="title">
                        	<?php
                        		echo($title);
                        	?>
                            </h4>                        
                        </div>								
            			<form action="?laman=edit" method="POST" enctype="">
							<div class="content">
								<div class="row">
		            				<div class="col-md-12">
		            					<div class="form-group">
											<label>Judul Halaman</label>
		            						<input type="text" class="form-control border-input" name="">
		            					</div>
		            				</div>	
		            			</div>		            			
								<div class="row">
		            				<div class="col-md-12">
		            					<div class="form-group">
											<label>Deskripsi Halaman</label>
		            						<textarea class="form-control border-input" rows="3"></textarea>
		            					</div>
		            				</div>	
		            			</div>			            			
								<div class="row">
		            				<div class="col-md-4">
		            					<div class="form-group">
											<label>Gambar Layout</label>
		            						<input  type="file" class="form-control border-input" name="">
		            					</div>
		            				</div>		            			
		            			</div>
		            			<div class="row text-center">
		            				<button type="submit" class="btn btn-info btn-fill btn-w">Ubah Data Halaman</button>
		            			</div>		            						            			
							</div>          				
            			</form>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</html>