<?php
    include '../web_machine/eng_db_con.php';
    $query  = "SELECT * FROM halcen_lamanutama";
    $result = mysqli_query($con, $query);
    $data   = mysqli_fetch_array($result);    

?>
<!DOCTYPE html>
<html lang="en">
<body>
    <div class="wrapper">
        <div class="page-header">
            <div class="page-header-image clear-filter" data-parallax="true" style="background-image: url('../assets/img/rektorat.jpg');">
            </div>
            <div class="container">
                <div class="content-center brand">
                    <img class="n-logo" src="<?php echo $data['logoUtama'];?>" alt="Main Logo">
                    <h1 class="h1-seo"><strong><?php echo $data['judulUtama'];?></strong></h1>
                    <h4><?php echo $data['deskripsiJudul'];?></h4>
                </div>
            </div>
        </div>      
        <!-- This Section is optional for image and other additional items -->
        <!-- <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <div class="hero-images-container">
                                <img src="../assets/img/headof.png" alt="">
                            </div>                            
                        </center>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="section section-about-us" data-background-color="green">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title"><?php echo $data['judulDesPendek'];?></h2>
                        <h4 class="description">
                        <?php echo $data['desPendek'];?>
                        </h4>
                    </div>
                </div>
            </div>            
        </div>        
        <div class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-6 text-center">
                        <h2 class="title">Visi</h2>
                        <?php echo $data['visi'];?>
                    </div>
                    <div class="col-6 text-center">
                        <h2 class="title">Misi</h2>
                        <?php echo $data['misi'];?>
                    </div>                                                              
                </div>                
            </div>
        </div>
        <div class="section" id="carousel" data-background-color="black">            
            <div class="container">
                <div class="col-8 mx-auto">
                    <h2 class="title text-center">Latest Info</h2>
                    <div id="carouselSlide" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselSlide" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselSlide" data-slide-to="1"></li>
                            <li data-target="#carouselSlide" data-slide-to="2"></li>
                            <li data-target="#carouselSlide" data-slide-to="3"></li>
                            <li data-target="#carouselSlide" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <a href="">
                                    <img class="d-block" src="../assets/img/bg1.jpg" alt="First slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Nature, United States</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block" src="../assets/img/bg3.jpg" alt="Second slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Somewhere Beyond, United States</h5>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block" src="../assets/img/bg4.jpg" alt="Third slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Yellowstone National Park, United States</h5>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block" src="../assets/img/bg4.jpg" alt="Fourth slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Yellowstone National Park, United States</h5>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block" src="../assets/img/bg4.jpg" alt="Fifth slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Yellowstone National Park, United States</h5>
                                </div>
                            </div>                                
                        </div>
                        <a class="carousel-control-prev" href="#carouselSlide" role="button" data-slide="prev">
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#carouselSlide" role="button" data-slide="next">
                            <i class="now-ui-icons arrows-1_minimal-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <h2 class="title">Organization</h2>
                <div class="team">
                    <div class="separator separator-primary"></div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="team-player">
                                <img src="../assets/img/avatar.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                                <h4 class="title">Romina Hadid</h4>
                                <p class="category text-primary">Model</p>
                                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                                    <a href="#">links</a> for people to be able to follow them outside the site.</p>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i backgrounr class="fa fa-twitter"></i></a>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i class="fa fa-instagram"></i></a>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i class="fa fa-facebook-square"></i></a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="team-player">
                                <img src="../assets/img/ryan.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                                <h4 class="title">Ryan Tompson</h4>
                                <p class="category text-primary">Designer</p>
                                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                                    <a href="#">links</a> for people to be able to follow them outside the site.</p>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i backgrounr class="fa fa-twitter"></i></a>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i class="fa fa-instagram"></i></a>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i class="fa fa-facebook-square"></i></a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="team-player">
                                <img src="../assets/img/eva.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                                <h4 class="title">Eva Jenner</h4>
                                <p class="category text-primary">Fashion</p>
                                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                                    <a href="#">links</a> for people to be able to follow them outside the site.</p>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i backgrounr class="fa fa-twitter"></i></a>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i class="fa fa-instagram"></i></a>
                                <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green"><i class="fa fa-facebook-square"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-basic" data-background-color="black">
            <div class="container">
                <div class="section-story-overview">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="image-container image-left" style="background-image: url('<?php echo $data['gbr2'];?>')">
                                <!-- First image on the left side -->
                                <p class="blockquote blockquote-primary">"This Qoutation about Halal Center. Halal is the main stuff that should had on Muslims consumption product."
                                    <br>
                                    <br>
                                    <small>-Wahyu Rudiyan Saputra</small>
                                </p>
                            </div>
                            <!-- Second image on the left side of the article -->
                            <div class="image-container" style="background-image: url('<?php echo $data['gbr3'];?>')"></div>
                        </div>
                        <div class="col-md-5">
                            <!-- First image on the right side, above the article -->
                            <div class="image-container image-right" style="background-image: url('<?php echo $data['gbr1'];?>')"></div>
                            <h3><?php echo $data['judulDesPelayanan'];?></h3>
                            <?php echo $data['desPelayanan'];?>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
        <div id="location">
            <script>
                function initMap() {
                    var coord = {lat: -0.0600159, lng: 109.3453186};
                    var map = new google.maps.Map(document.getElementById('location'), {
                    zoom: 17,
                    center: coord,
                    mapTypeId: 'hybrid'
                    });
                    var marker = new google.maps.Marker({
                    position: coord,
                    map: map
                    });
                }
            </script>
            <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYorEJZlB4ItQ_FW9GzkOrIWYqeAGMXKA&callback=initMap">
            </script>
        </div>
        <div class="section section-basic content-center" data-background-color="green">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="title text-center">Contact Us</h4>
                        <ul style="list-style-type: none;">
                            <li>
                                <i class="now-ui-icons users_circle-08 contact-us-icons"></i>
                                <p class="contact-us">(0561) 739630</p>
                            </li>
                            <li>
                                <i class="now-ui-icons ui-1_email-85 contact-us-icons"></i>
                                <p class="contact-us">halcen@dept.untan.ac.id</p>
                            </li>
                            <li>
                                <i class="now-ui-icons location_map-big contact-us-icons"></i>
                                <p class="contact-us">Jl. Prof. Dr. Hadari Nawawi, <br>Pontianak, Kalimantan Barat</p>
                            </li>                            
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h4 class="title text-center">Our Partner</h4>
                        <!-- <img src="http://humas.pontianakkota.go.id/images/logokotapontianak.png" alt="Thumbnail Image" class="img-fluid"> -->
                        <img src="http://2.bp.blogspot.com/-MH4Xd1k2sR0/UOl-HiSw5EI/AAAAAAAAKQc/OgdfbE_F93E/s1600/LOGO+BADAN+POM.png" alt="Thumbnail Image" class="img-fluid">
                        <img src="https://upload.wikimedia.org/wikipedia/id/f/f5/Logo_MUI.png" alt="Thumbnail Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>    
</body>
</html>