<?php 
  include '../web_machine/eng_db_con.php';
  include '../web_machine/pagination.php';
?>

<div class="landing-page sidebar-collapse">
  <div class="wrapper">
    <div class="head-space"></div>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-8">
          <h1 class="my-4">Halal Center            
            <small> Berita dan Informasi</small>
          </h1>

          <?php 
            while ($row = mysqli_fetch_assoc($result)) {
              # code...
          ?>    

          <!-- Blog Post -->
          <div class="crd mb-4">
            <img class="crd-img-top" src="<?php echo $row['image']; ?>" alt="crd image cap">
            <div class="crd-body">
              <h2 class="crd-title"><?php echo $row['judulPost']; ?></h2>
              <p class="crd-text"><?php echo $row['deskripsi']; ?></p>
              <a href="?laman=view&title=<?php echo $row['judulPost']; ?>" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="crd-footer text-muted">
              Posted on <?php echo date('d M Y', strtotime($row['create_at'])); ?> by
              <a href="#"><?php echo $row['nama_depan']." ".$row['nama_belakang']; ?></a>
            </div>
          </div>

          <?php
            }
          ?>         

          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            <div class="col-md-4">
              <nav aria-label="Page navigation example">
                  <ul class="pagination pagination-success">
                      <?php for ($i=1; $i <= $pages ; $i++) { 
                        # code...
                      ?>
                      <li class="page-item active">
                          <a class="page-link" href="?laman=post&page=<?php echo $i ?>"><?php echo $i ?></a>
                      </li>
                     <?php } ?>
                  </ul>
              </nav>
          </div>
        </div>

        <?php include 'sidebar_widget.php'; ?>

      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->               
  </div>
</div>