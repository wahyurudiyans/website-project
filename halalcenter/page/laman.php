<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/now-ui-kit.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
    <!-- This link for ours CSS -->
    <link href="../assets/css/design.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-social.css" rel="stylesheet">
</head>

<?php $lamanData = $_GET['laman']; ?>

<body class="index-page sidebar-collapse">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-primary fixed-top 
    <?php
        if($lamanData != 'post' and $lamanData != 'view'){
            echo 'navbar-transparent';
        }
    ?> 
    "
    <?php
        if($lamanData != 'post' and $lamanData != 'view' and $lamanData == 'main'){
            echo 'color-on-scroll="650"';
        }
        elseif($lamanData != 'post' and $lamanData != 'view'){
            echo 'color-on-scroll="200"';
        }
    ?>
    >
      <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="?laman=main" rel="tooltip" title="Halal Center Tanjungpura University" data-placement="bottom">
                    <img class="main-logo" src="../assets/img/samplelogo.png" alt="">                    
                </a>
                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="./assets/img/blurred-image-1.jpg">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="?laman=services">
                            <i class="now-ui-icons shopping_delivery-fast"></i>
                            <p>Pelayanan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?laman=research">
                            <i class="now-ui-icons education_atom"></i>
                            <p>Penelitian</p>
                        </a>
                    </li>          
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="now-ui-icons business_bank"></i>
                            <p>Fasilitas</p>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="?laman=lab">Laboratory</a>
                            <a class="dropdown-item" href="?laman=devices">Device and Equipment</a>
                        </div>
                    </li>                                        
                    <li class="nav-item">
                        <a class="nav-link" href="?laman=post&page=1">
                            <i class="now-ui-icons education_paper"></i>
                            <p>Berita</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?laman=about">
                            <i class="now-ui-icons sport_user-run"></i>
                            <p>Tentang Kami</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
 
    <?php
        switch($_GET['laman']) {
            case 'post':
                # code...
                include 'posts.php';
                break;            

            case 'services':
                # code...
                require_once 'landing.php';
                break;
                
            case 'research':
                # code...
                require_once 'landing.php';
                break;
                
            case 'lab':
                # code...
                require_once 'landing.php';
                break;
                
            case 'devices':
                # code...
                require_once 'landing.php';
                break;

            case 'about':
                # code...
                require_once 'landing.php';
                break;

            case 'coba':
                # code...
                require_once 'berita.php';
                break;

            case 'view':
                # code...
                $judul = $_GET['title'];
                require_once 'berita.php';
                break;                

            default:
                # code...
                require_once 'main.php';
                break;
        }
    ?>

    <footer class="footer" <?php if($_GET['laman'] != 'main') {echo 'data-background-color="green"';}?>>
        <div class="container">
            <nav class="bot-font">
                <ul class="ml-auto mr-auto">
                    <li class="nav-item">
                        <a title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
                            <i class="fa fa-twitter"></i>
                            <p class="d-lg-none d-xl-none">Twitter</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
                            <i class="fa fa-facebook-square"></i>
                            <p class="d-lg-none d-xl-none">Facebook</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
                            <i class="fa fa-instagram"></i>
                            <p class="d-lg-none d-xl-none">Instagram</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="copyright mt-2 mb-2">
                &copy;
                <script>
                    document.write(new Date().getFullYear())
                </script>, Website by
                <a href="#" target="_blank">Wahyu Rudiyan S</a>.
            </div>
        </div>
    </footer>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<script src="../assets/js/calendar.js" type="text/javascript"></script>
</html>