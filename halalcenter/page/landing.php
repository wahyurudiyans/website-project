<!DOCTYPE html>
<html lang="en">
<body class="landing-page sidebar-collapse">
    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/lab.jpg');">
            </div>
            <div class="container">
                <div class="content-center">
                    <h2 class="title">Title of Page Here</h2>
                    <h4>This part for descriptions.</h4>
                    <!-- <div class="text-center">
                        <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="#pablo" class="btn btn-neutral btn-icon btn-round" data-background-color="green">
                            <i class="fa fa-google-plus"></i>
                        </a>
                            
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="section section-about-us">
            <div class="container">
                <div class="section-story-overview">
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <h3 class="title">So what does the new record for the lowest level of winter ice actually mean</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <!-- First image on the right side, above the article -->
                            <div class="image-container image-center" style="background-image: url('../assets/img/bg1.jpg')">
                                

                            </div>
                        </div>
                    </div>              
                    <div class="row">
                        <div class="col-md-10 mx-auto mt-5">                                                        
                            <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
                            </p>
                            <p>
                                For a start, it does not automatically follow that a record amount of ice will melt this summer. More important for determining the size of the annual thaw is the state of the weather as the midnight sun approaches and temperatures rise. But over the more than 30 years of satellite records, scientists have observed a clear pattern of decline, decade-by-decade.
                            </p>
                            <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
                            </p>
                        </div>
                    </div>

                    <!-- Blog Post -->
                    <!-- <div class="row">
                        <div class="col-md-10 mx-auto">
                            <div class="crd mb-4">
                                <img class="crd-img-top" src="http://placehold.it/750x300" alt="crd image cap">
                                <div class="crd-body">
                                    <h2 class="crd-title">Post Title</h2>
                                    <p class="crd-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                                </div>
                                <div class="crd-footer text-muted">
                                    Posted on January 1, 2017 by
                                    <a href="#">Start Bootstrap</a>
                                </div>
                            </div>                            
                        </div>
                    </div> -->

                </div>
            </div>
        </div>
    </div>
</body>
</html>