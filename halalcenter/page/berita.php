<?php 
    include '../web_machine/eng_db_con.php';

    $query = "SELECT * FROM halcen_posts JOIN halcen_user WHERE halcen_posts.id_author = halcen_user.id and judulPost='$judul'";
    $result = mysqli_query($con, $query);
    $post = mysqli_fetch_array($result);
?>

<div class="landing-page sidebar-collapse">
  <div class="wrapper">
    <div class="head-space"></div>
    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Title -->
          <h1 class="mt-4"><?php echo $post['judulPost']; ?></h1>

          <!-- Author -->
          <p class="lead">
            by
            <a href="#"><?php echo $post['nama_depan'].' '.$post['nama_belakang'];?></a>
          </p>

          <hr>

          <!-- Date/Time -->
          <p>Posted on <?php echo date('d M Y', strtotime($post['create_at'])); ?></p>

          <hr>

          <!-- Preview Image -->
          <img class="post-img" src="<?php echo $post['image']; ?>" alt="">

          <hr>

          <!-- Post Content -->
          <div class="content">
            <div class="row">
              <?php echo $post['konten']; ?>
            </div>            
          </div>

          <hr>          

        </div>

        <?php include 'sidebar_widget.php'; ?>

      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->        
  </div>
</div>