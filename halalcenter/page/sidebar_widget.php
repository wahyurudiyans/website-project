<?php 
    $query = "SELECT * FROM halcen_posts";
    $result = mysqli_query($con, $query);    
?>

<!-- Sidebar Widgets Column -->
<div class="col-md-4">
    <!-- Search Widget -->
    <div class="crd my-4">
        <h5 class="crd-header">Search</h5>
        <div class="crd-body">
            <input name="search" type="text" class="form-control" placeholder="Cari..." style="height: 45px;">
            <div class="row mx-auto">
                <button class="btn btn-round btn-block" type="submit">Cari</button>
            </div>
        </div>
    </div>

    <div class="crd my-4">
        <h5 class="crd-header">Socials</h5>
        <div class="crd-body">
            <a class="btn btn-block btn-lg btn-social btn-facebook">
                <span class="fa fa-facebook"></span> Find Us on Facebook
            </a>
            <a class="btn btn-block btn-lg btn-social btn-twitter">
                <span class="fa fa-twitter"></span> Find Us on Twitter
            </a>
            <a class="btn btn-block btn-lg btn-social btn-instagram">
                <span class="fa fa-instagram"></span> Find Us on Instagram
            </a>
        </div>
    </div>    

    <div class="crd my-4">
        <h5 class="crd-header">Calendar</h5>
        <div class="crd-body">
            <div class="calendar calendar-first" id="calendar_first">
                <div class="calendar_header">
                    <button class="switch-month switch-left"> <i class="fa fa-chevron-left"></i></button>
                     <h2></h2>
                    <button class="switch-month switch-right"> <i class="fa fa-chevron-right"></i></button>
                </div>
                <div class="calendar_weekdays"></div>
                <div class="calendar_content"></div>
            </div>
        </div>
    </div> 

    <!-- Categories Widget --> 
    <!-- <div class="crd my-4">
        <h5 class="crd-header">Categories</h5>
        <div class="crd-body">
            <div class="row">
                <div class="col-lg-6">
                    <ul class="list-unstyled mb-0">
                    <?php 
                        while ($row = mysqli_fetch_array($result)) {
                            # code...
                    ?>
                        <a href="#"><?php echo $row['category']; ?></a>
                    <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Side Widget -->
    <!-- <div class="crd my-4">
        <h5 class="crd-header">Side Widget</h5>
        <div class="crd-body">
            You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 crd containers!
        </div>
    </div> -->
</div>