<?php
    CREATE TABLE halcen_user(   
                                id int AUTO_INCREMENT NOT NULL,
                                username varchar(35) NOT NULL,
                                password varchar(35) NOT NULL,
                                role varchar(25),
                                nama_depan varchar(35),
                                nama_belakang varchar(35),
                                jabatan varchar(35),
                                email varchar(100),
                                at_create datetime,
                                at_update datetime,
                                PRIMARY KEY (ID)
                            );

    CREATE TABLE halcen_lamanUtama(
                                id int AUTO_INCREMENT NOT NULL,
                                logoUtama varchar(200) NOT NULL,
                                judulUtama varchar(60) NOT NULL,
                                deskripsiJudul varchar(100),
                                judulDesPendek varchar(100),
                                desPendek TEXT,
                                visi TEXT,
                                misi TEXT,
                                judulDesPelayanan varchar(100),
                                gbr1 varchar(200),
                                gbr2 varchar(200),
                                gbr3 varchar(200),
                                telepon varchar(20),
                                email varchar(250),
                                alamat varchar(150),
                                at_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                PRIMARY KEY (ID)    
                            );

    CREATE TABLE halcen_posts(   
                                id int AUTO_INCREMENT NOT NULL,
                                judulPost varchar(250) NOT NULL,
                                category varchar(35) NOT NULL,                                
                                konten TEXT,
                                deskripsi TEXT,
                                image varchar(250),
                                id_author int,
                                create_at TIMESTAMP NOT NULL,                                
                                PRIMARY KEY (ID)
                            );

    "ALTER TABLE halcen_posts DROP FOREIGN KEY halcen_posts_ibfk_1; ALTER TABLE halcen_posts ADD CONSTRAINT Relasi Postingan dan User FOREIGN KEY (id_author) REFERENCES halcen_user(id) ON DELETE NO ACTION ON UPDATE NO ACTION;"

?>